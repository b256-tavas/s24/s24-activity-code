// console.log("hello")




// let address1 = address[0];
// let address2 = address[1];
// let address3 = address[2];
// let address4 = address[3];
// let address5 = address[4];





// Session 24 Activity Instructions:
// In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// Link the index.js file to the index.html file.
let getCube = 7 ** 3;

let cube = `The cube of 7 is ${getCube}`;
console.log(cube);


// Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)


// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let address = [1101, "Paprika Lane", "Dela Paz", "Pasig City", "Philippines"];


let [address0, address1, address2, address3, address4] = address
console.log("I live at "+ address0 + " " + address1 + " "  + address2 + " " + address3 + " " + address4)


// Create a variable address with a value of an array containing details of an address.
// Destructure the array and print out a message with the full address using Template Literals.



let chicken = {
	name: 'chicken',
	parts: 'wings',
	ability: 'fly',
	advantage: 'cockfighting'
}

let {name, parts, ability, advantage} = chicken
console.log("The" + " " + name + " " + "can" + " "+ ability + " " + "using its strong and enormous" + " " + parts + " " + "when" + " " + advantage + ".")

// let dog = {
// 	name: 'doggie',
// 	parts: 'snout',
// 	ability: 'bark',
// 	advantage: 'running'
// }

// let dogDetails = `The ${dog.name} can ${dog.ability} when ${dog.advantage} using its big and fierce ${dog.parts}.`
// console.log(dogDetails)

// let giraffe = {
// 	name: 'giraffe',
// 	parts: 'neck',
// 	ability: 'swing',
// 	advantage: 'running'
// }

// let giraffeDetails = `The ${giraffe.name} can ${giraffe.ability} its ${giraffe.parts} when ${giraffe.advantage} while angry.`
// console.log(giraffeDetails);




// Create a variable animal with a value of an object data type with different animal details as it’s properties.
// Destructure the object and print out a message with the details of the animal using Template Literals.

let numbers = [1, 2, 3, 4, 5];

numbers.forEach(function(number) {
	console.log(number);
})

let reducedNumber = numbers.reduce((x, y) => {

	return x + y;
})

console.log(reducedNumber)

// Create an array of numbers.
// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.


class Dog {
	constructor(breed, age, color) {

		this.breed = breed;
		this.age = age;
		this.color = color;
	}
}

let dog1 = new Dog("Labrador", 5, "Brown");
console.log(dog1);
let dog2 = new Dog("Belgian Malinois", 8, "Black");
console.log(dog2);
let dog3 = new Dog("German Shepherd", 15, "Yellow");
console.log(dog3);



// Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// Create/instantiate a new object from the class Dog and console log the object.
// Create a git repository named S24.
// Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// Add the link in Boodle.